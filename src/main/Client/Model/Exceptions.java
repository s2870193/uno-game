package main.Client.Model;

public class Exceptions {
    public static class NotAValidSyntax extends Exception{
        public NotAValidSyntax(String message){
            super(message);
        }
    }
}
