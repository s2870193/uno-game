package main.Client.Controller.Handlers;

import main.Server.Controller.NetworkProtocol.Functionality;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable{
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public ClientHandler (Socket socket) {
        this.socket = socket;
        try {
          this.in = new BufferedReader (new InputStreamReader (socket.getInputStream ()));
          this.out = new PrintWriter (new OutputStreamWriter (socket.getOutputStream ()), true);
        } catch (IOException e) {
            System.out.println (e.getStackTrace ());
        }
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run () {
        while(true) {
            try {
                String response = in.readLine ();
                handleResponse(response);

            } catch (IOException e) {
                System.out.println (e.getStackTrace ());
            }
        }
    }

    public void handleUserInput(String input) {
        String[] s = input.split (" ");
        switch (s[0]) {
            case "CONNECTED" :{
                System.out.println ("CONNECTED!");
                break;
            }
            case "QUEUE_JOINED": {
                break;
            }
            case "QUEUE_LEFT": {
                break;
            }
            case "GAME_STARTED": {
                break;
            }
            case "ROUND_STARTED": {
                break;
            }
            case "NEXT_TURN" : {
                break;
            }
            case "ISSUE_CARD": {
                break;
            }
            case "ROUND_FINISHED": {
                break;
            }
            case "GAME_FINISHED": {
                break;
            }
            case "ERROR": {
                break;
            }
            case "CHAT": {
                break;
            }
            case "HI": {
                out.println ("HI" + s[1] + s[2] + "1.2");
            }
        }
    }

    private void handleResponse (String response) {
        switch (response) {
            case "HI": {
                break;
            }
            case "JOIN_GAME": {
                break;
            }
            case "PLAY_CARD": {
                break;
            }
            case "DRAW_CARD": {
                break;
            }
            case "CHAT": {
                break;
            }
        }
    }


}
