package main.Client.Controller.Handlers;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import main.Server.Controller.Table.TableController;
import main.Server.View.TUI;

public class Client implements Runnable{

    private TableController ipPort;
    public static void main (String[] args) {
        Client client = new Client ();
        new Thread(client).start ();
    }
    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run () {
        try {
            TUI.askForPort ();
        } catch (InterruptedException e) {
            throw new RuntimeException (e);
        }
        Scanner newPort = new Scanner(System.in);
        int port = Integer.parseInt (newPort.nextLine ());
        try {

            Socket socket = new Socket("localhost", port);  //Ask for this for the client.
            ClientHandler clientHandler = new ClientHandler (socket);
            new Thread(clientHandler).start ();

            BufferedReader in = new BufferedReader (new InputStreamReader ( socket.getInputStream ()));
            PrintWriter out =  new PrintWriter (new OutputStreamWriter (socket.getOutputStream ()), true);
            Scanner scanner = new Scanner (System.in);

            System.out.println ("Enter name: ");
            String username = scanner.nextLine ();
            String userInput = "";

            do{
                userInput = scanner.nextLine ();
                out.println ("Client ("+ username +"): -> " + userInput);

            }while(!userInput.equals ("Exit"));

        } catch (IOException e) {
            System.out.println (e.getStackTrace ());
        }
    }

    public String getIpPort () {
        return getIpPort();
    }
}
