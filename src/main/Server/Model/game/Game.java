package main.Server.Model.game;

import main.Server.Controller.Game.GameMode;
import main.Server.Model.player.Player;
import main.Server.Model.table.Table;

import java.util.ArrayList;
import java.util.Scanner;

public class Game{

    private ArrayList<Player> players;
    private Table table;
    private GameMode mode;

    private boolean over;

    public <E extends GameMode> Game(E e, ArrayList<Player> players){
        this.mode = e;
        this.mode.setGame (this);
        this.players = players;
        this.table = new Table(this.players);
        this.table.setGame (this);
        this.mode.start ();
        this.over = false;
        // new Game(new NormalMode(), players)
    }

    public ArrayList<Player> getPlayers () {
        return players;
    }


    public Table getTable () {
        return table;
    }

    public GameMode getMode () {
        return mode;
    }


    public boolean isOver () {
        return over;
    }

    public void setOver (boolean over) {
        this.over = over;
    }
}
