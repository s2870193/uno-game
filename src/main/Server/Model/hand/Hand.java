package main.Server.Model.hand;

import main.Server.Model.card.Card;

import java.util.ArrayList;

public class Hand {

    private ArrayList<Card> cards;

    public Hand() {
        this.cards = new ArrayList<> ();
    }

    public ArrayList<Card> getCards () {
        return cards;
    }

    public void setCards (ArrayList<Card> cards) {
        this.cards = cards;
    }

    public void addCard(Card card){
        this.cards.add (card);
    }
}
