package main.Server.Model.table;

import main.Server.Controller.Table.TableController;
import main.Server.Model.deck.Deck;
import main.Server.Model.game.Game;
import main.Server.Model.player.Player;

import java.util.ArrayList;

public class Table {

    private ArrayList<Player> players;
    private Player currentPlayer;
    private Game game;
    private TableController controller;
    private Deck deck;
    private boolean direction;


    public Table(ArrayList<Player> players){
        this.players = players;
        this.deck = new Deck ();
        this.controller = new TableController (this);
    }


    public boolean isDirection () {
        return direction;
    }

    public void setDirection (boolean direction) {
        this.direction = direction;
    }

    public void handleInput(String input){
        this.controller.handleInput (input);
    }

    public ArrayList<Player> getPlayers () {
        return players;
    }

    public void setPlayers (ArrayList<Player> players) {
        this.players = players;
    }

    public Game getGame () {
        return game;
    }

    public void setGame (Game game) {
        this.game = game;
    }

    public Deck getDeck () {
        return deck;
    }

    public void setDeck (Deck deck) {
        this.deck = deck;
    }

    public Player getCurrentPlayer () {
        return currentPlayer;
    }

    public void setCurrentPlayer (Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}
