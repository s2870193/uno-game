package main.Server.Model.deck;

import main.Server.Model.card.Card;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class Deck {
    private ArrayList<Card> cards;

    private ArrayList<Card> discardedCards;

    public Deck () {
        this.cards = new ArrayList<Card>();
        this.cards = generateCards ();
        shuffle ();
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }

    public Card draw() {
        if (this.cards.size() > 0) {
            return this.cards.remove(0);
        }
        return null;
    }

    public int getSize() {
        return this.cards.size();
    }

    public ArrayList<Card> getCards () {
        return cards;
    }

    public void setCards (ArrayList<Card> cards) {
        this.cards = cards;
    }

    public ArrayList<Card> getDiscardedCards () {
        return discardedCards;
    }

    public void setDiscardedCards (ArrayList<Card> discardedCards) {
        this.discardedCards = discardedCards;
    }

    public ArrayList<Card> generateCards() {
        ArrayList<Card> cards = new ArrayList<> ();
        for (String color : Card.COLOURS){
            for(String value : Card.VALUES){
                cards.add (new Card (color,value));
            }
        }
        for (int i = 0; i <= 3; i++) {
            cards.add (new Card ("W","W"));
        }
        for (int i = 0; i <= 3; i++) {
            cards.add (new Card ("W", "F"));
        }
        return cards;
    }

    public Card getTopCard() {
        return this.discardedCards.get (this.discardedCards.size ()-1);
    }
}
