package main.Server.Model.player;

import main.Server.Model.hand.Hand;

public  abstract class Player {

    private String name;
    private Hand hand;

    private int score;

    public Player(String name){
        this.name = name;
        this.score = 0;
        this.hand = new Hand ();
    }


    public abstract String makeMove();

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Hand getHand () {
        return hand;
    }

    public void setHand (Hand hand) {
        this.hand = hand;
    }

    public int getScore () {
        return score;
    }

    public void setScore (int score) {
        this.score = score;
    }
}
