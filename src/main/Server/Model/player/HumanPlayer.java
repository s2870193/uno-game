package main.Server.Model.player;

import java.util.Scanner;

public class HumanPlayer extends Player {


    public HumanPlayer (String name) {
        super (name);
    }

    @Override
    public String makeMove () {
        Scanner input = new Scanner (System.in);
        String newInput = input.nextLine ();
        return newInput;
    }

    //Make checks for inalid moves.
}
