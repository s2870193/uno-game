package main.Server.Model.card;

public class Card {
    private String Color, Value, defaultColor;
    public static final String[] COLOURS = {"R", "Y", "G", "B", "W"};
    public static final String[] VALUES = {"ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "S", "R", "D", "W" ,"F"};

    public Card (String COLOUR, String VALUE) {
        this.Color = COLOUR;
        this.Value = VALUE;
        this.defaultColor = COLOUR;
    }

    public String getColor () {
        return Color;
    }

    public String getValue () {
        return Value;
    }

    public void setColor (String color) {
        Color = color;
    }
    public String getDefaultColor () {
        return defaultColor;
    }
}
 /*
    S - Skip src.main.Client.Model.card
    R - Reverse src.main.Client.Model.card
    D - Draw two
    W - Wild src.main.Client.Model.card
    F - Wild src.main.Client.Model.card + 4
 */