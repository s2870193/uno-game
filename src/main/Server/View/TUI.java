package main.Server.View;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import main.Server.Model.card.Card;
import main.Server.Model.hand.Hand;
import main.Server.Model.player.Player;
import main.Server.Model.table.Table;

public class TUI {

    private Hand hand;

    public Hand getHand () {
        return hand;
    }

    public static void greetings() throws InterruptedException {
        System.out.println ("Welcome to UNO made on Java!");
        TimeUnit.NANOSECONDS.sleep (1000);
    }

    public static void createOrJoin() throws InterruptedException {
        System.out.println ("This game has the ability to create or join a multiplayer sesion.");
        TimeUnit.NANOSECONDS.sleep (300);
        System.out.println ("The available modes are 'join' and 'create'.");
        TimeUnit.NANOSECONDS.sleep (300);
        System.out.println ("The syntax for join is 'join command port'");
        TimeUnit.NANOSECONDS.sleep (300);
        System.out.println ("The syntax for join is 'create gameMode port amountOfPlayers'");
        System.out.println ("(gameMode available 'NormalGame' max amount of player '10')");
        TimeUnit.NANOSECONDS.sleep (500);
        System.out.print ("Your choice : ");
    }
    public static void askForPlayers() throws InterruptedException {
        System.out.println ("How mane players do you want to initialize ? ");
        TimeUnit.NANOSECONDS.sleep (500);
        System.out.print ("Players : ");
    }

    public static void askForNames() throws InterruptedException {
        System.out.println ("What is the name you wish to provide for the player ?");
        TimeUnit.NANOSECONDS.sleep (500);
        System.out.print ("Name : ");
    }

    public static void askForGamemode() throws InterruptedException {
        System.out.println ("What is the game mode you wish to play ?");
        TimeUnit.NANOSECONDS.sleep (500);
        System.out.println ("The available game modes are: NormalGame and SevenUno");
        TimeUnit.NANOSECONDS.sleep (200);
        System.out.print ("Game mode : ");
    }

    public static void askForPort() throws  InterruptedException{
        System.out.println ("Which port do you want to bind to localholst ?");
        TimeUnit.NANOSECONDS.sleep (500);
        System.out.print ("Port : ");
    }

    public static void printCards(Hand hand) throws InterruptedException {
        ArrayList<Card> currentHand = hand.getCards ();
        System.out.println ("This is you deck of cards: "+currentHand);
        System.out.println ("What card do you want to play?");
        TimeUnit.NANOSECONDS.sleep (200);
        System.out.print ("Card: ");
    }
}
