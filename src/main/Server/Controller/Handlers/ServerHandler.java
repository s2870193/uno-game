package main.Server.Controller.Handlers;

import main.Server.Model.player.Player;

import java.io.*;
import java.net.Socket;

public class ServerHandler implements Runnable{

    private Server server;
    private Socket socket;
    private Player player;
    private BufferedReader in;
    private PrintWriter out;


    public ServerHandler (Socket socket,Server server) {
        this.server = server;
        this.socket = socket;
        try {
            this.in = new BufferedReader (new InputStreamReader (socket.getInputStream ()));
            this.out = new PrintWriter (new OutputStreamWriter (socket.getOutputStream ()), true);
        } catch (IOException e) {
            throw new RuntimeException (e);
        }
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run () {
        try {
            while(true) {
                String output = in.readLine ();
                handleResponse (output);
                if(output.equals ("Exit")) {
                    break;
                }
                for (ServerHandler sh : this.server.getClients ()){
                    sh.out.println (output);
                }

            }
        } catch (IOException e) {
            System.out.println (e.getStackTrace ());
        }
    }
    private void handleResponse (String response) {
        switch (response) {
            case "CONNECTED" :{
                System.out.println ("CONNECTED!");
                break;
            }
            case "QUEUE_JOINED": {
                break;
            }
            case "QUEUE_LEFT": {
                break;
            }
            case "GAME_STARTED": {
                break;
            }
            case "ROUND_STARTED": {
                break;
            }
            case "NEXT_TURN" : {
                break;
            }
            case "ISSUE_CARD": {
                break;
            }
            case "ROUND_FINISHED": {
                break;
            }
            case "GAME_FINISHED": {
                break;
            }
            case "ERROR": {
                break;
            }
            case "CHAT": {
                break;
            }
        }
    }


}
