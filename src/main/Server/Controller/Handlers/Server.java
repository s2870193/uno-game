package main.Server.Controller.Handlers;

import main.Server.Controller.NetworkProtocol.Command;
import main.Server.Controller.Table.TableController;
import main.Server.View.TUI;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Server implements Runnable{
    private ArrayList<ServerHandler> clients;
    private String ipPort;
    public Server () {
        this.clients = new ArrayList<> ();
    }

    public static void main (String[] args) {
        Server server = new Server ();
        new Thread (server).start ();
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run () {

        System.out.println ("Server has Started! ");
        try {
            TUI.askForPort ();
        } catch (InterruptedException e) {
            throw new RuntimeException (e);
        }
        Scanner newPort = new Scanner(System.in);
        int port = Integer.parseInt (newPort.nextLine ());
        try {
            ServerSocket serverSocket = new ServerSocket (port);    //Make it so it asks for the protocol. Done!
            System.out.println ("Listening on port !");
            while(clients.size () <= 10) {
                Socket socket = serverSocket.accept ();

                System.out.println (Command.CONNECTED);
                ServerHandler serverHandler = new ServerHandler (socket,this);
                clients.add (serverHandler);
                new Thread (serverHandler).start ();
            }
        } catch (IOException e) {
            System.out.println (e.getStackTrace ());
        }
    }


    public ArrayList<ServerHandler> getClients () {
        return clients;
    }

    public String getIpPort () {
        return getIpPort();
    }

    public void setIpPort (String ipPort) {
        this.ipPort = ipPort;
    }
}
