package main.Server.Controller.Table;

import main.Server.Controller.NetworkProtocol.Command;
import main.Server.Model.card.Card;
import main.Server.Model.deck.Deck;
import main.Server.Model.player.Player;
import main.Server.Model.table.Table;
import main.Server.View.TUI;
import org.json.JSONObject;

import java.util.ArrayList;

import static java.lang.System.out;

public class TableController {

    private Table table;

    public TableController(Table table){
        this.table = table;
        this.table.setCurrentPlayer (table.getPlayers ().get (0));
    }

    public void handleInput(String input){
        JSONObject jo = new JSONObject();
        String[] split = input.split (" ");

        switch(split[0]) {
        case "play": {
            Card cardToPlay = table.getCurrentPlayer ().getHand ().getCards().get (Integer.parseInt (split[1])-1);
            if(cardToPlay.getDefaultColor ().equals ("W")) {
                if(split.length != 3){
                    String newInput = this.table.getCurrentPlayer ().makeMove ();
                    handleInput(newInput);
                    if(cardToPlay.getValue ().equals ("W")) {
                        cardToPlay.setColor (split[2]);
                        table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                        table.getDeck ().getDiscardedCards ().add (cardToPlay);
                        nextPlayer ();
                    } else if (cardToPlay.getValue ().equals ("F")) {
                        cardToPlay.setColor (split[2]);
                        table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                        table.getDeck ().getDiscardedCards ().add (cardToPlay);
                        nextPlayer ();
                        giveFour (table.getCurrentPlayer ());
                        nextPlayer ();
                    }
                }
            }
            if(cardToPlay.getColor ().equals (table.getDeck ().getTopCard ()) || cardToPlay.getValue ().equals (table.getDeck ().getTopCard ())) {
                if(cardToPlay.getValue ().equals ("R")) {
                    reverse ();
                    table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                    table.getDeck ().getDiscardedCards ().add (cardToPlay);
                    nextPlayer ();
                }
                if (cardToPlay.getValue ().equals ("S")) {
                    table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                    table.getDeck ().getDiscardedCards ().add (cardToPlay);
                    nextPlayer ();
                    nextPlayer ();
                }
                if(cardToPlay.getValue ().equals ("D")) {
                    table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                    table.getDeck ().getDiscardedCards ().add (cardToPlay);
                    nextPlayer ();
                    giveTwo (table.getCurrentPlayer ());
                    nextPlayer ();
                }
                table.getCurrentPlayer ().getHand ().getCards ().remove (cardToPlay);
                table.getDeck ().getDiscardedCards ().add (cardToPlay);
                nextPlayer ();
            }
            String newInput = this.table.getCurrentPlayer ().makeMove ();
            handleInput(newInput);
            break;
        }
        case "draw": {
            Card newCard = this.table.getDeck ().draw ();
            table.getCurrentPlayer ().getHand ().addCard (newCard);
            break;
        }
    }
}

    /*
    Get the current card, check if the current card is a valid card based on the card on the table, if so
    discard the card and go to the next player, if not, ask the player again, give an option for grabbing cards
    from the deck.

    For every special card, proceed to the corresponding method accordingly.
     */

    public Player nextPlayer(){
        Player currentPlayer = table.getCurrentPlayer ();
        if(table.isDirection ()) {
            this.table.setCurrentPlayer (table.getPlayers ().get ((table.getPlayers ().indexOf (currentPlayer) + 1) % this.table.getPlayers ().size ()));
        } else {
            if(table.getPlayers ().indexOf (currentPlayer) - 1 >= 0) {
                this.table.setCurrentPlayer (table.getPlayers ().get ((table.getPlayers ().indexOf (currentPlayer) - 1) % this.table.getPlayers ().size ()));
            } else {
                this.table.setCurrentPlayer (table.getPlayers ().get (table.getPlayers ().size ()- 1));
            }
        }
        return currentPlayer;
    }

    public void giveInitialCards(){
        Deck deck = new Deck ();
        ArrayList<Player> playersList = table.getPlayers ();
        for (Player player : playersList) {
            for (int i = 0; i < 7; i++) {
                player.getHand ().getCards ().add (deck.draw ());
            }
        }
    }

    public void giveTwo(Player player) {
        Player currentPlayer = table.getCurrentPlayer ();
        for (int i = 0; i < 1; i++) {
            player.getHand ().getCards ().add (table.getDeck ().draw ());
        }
    }

    public void giveFour(Player player){
        Player currentPlayer = table.getCurrentPlayer ();
        for (int i = 0; i < 3; i++) {
            player.getHand ().getCards ().add (table.getDeck ().draw ());
        }
    }

    public void drawCard() {
        table.getCurrentPlayer ().getHand ().addCard (table.getDeck ().draw ());
    }

    public void reverse() {
        this.table.setDirection (!this.table.isDirection ());
    }
}
