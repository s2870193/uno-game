
package main.Server.Controller.Game;

import main.Server.Model.deck.Deck;
import main.Server.Model.player.ComputerPlayer;
import main.Server.Model.player.HumanPlayer;
import main.Server.Model.player.Player;
import main.Server.Model.table.Table;
import main.Server.View.TUI;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class NormalGame extends GameMode{
    private TUI tui;
    public NormalGame(){
    }

    /**
     * starts the src.main.Client.Model.game
     */

    /**
     * @requires game, isOver, table, currentPlayer, makeMove
     * @ensure game can start
     */
    @Override
    public void start () {
        while(!super.getGame ().isOver ()){
            String input = super
                    .getGame ()
                    .getTable ()
                    .getCurrentPlayer ()
                    .makeMove ();

            super.getGame ()
                    .getTable ()
                    .handleInput (input);
        }
    }

    /**
     * ends the src.main.Client.Model.game
     */
    @Override
    public void end () {

    }

    /**
     * gets the state of the src.main.Client.Model.game
     */
    @Override
    public void getState () {

    }


}