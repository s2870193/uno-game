package main.Server.Controller.Game;

import main.Server.Model.game.Game;

public abstract class GameMode {

    private Game game;

    public Game getGame () {
        return this.game;
    }

    public void setGame (Game game) {
        this.game = game;
    }

    /**
     *  starts the src.main.Client.Model.game
     */
    public abstract void start();

    /**
     *  ends the src.main.Client.Model.game
     */
    public abstract void end();

    /**
     *  gets the state of the src.main.Client.Model.game
     */
    public abstract void getState();
}
