# Uno Game in Java.

To start the Uno Game the user needs to run the Server.class file to start the server at the moment.
To play with a client run the Client.class file. Client can be opened multiple times for multiple players.

Different commands are available for the server based on the following protocol:

-	https://gitlab.utwente.nl/s3032132/uno-protocol

Reading the information in the protocol, the aformention commands are implemented in the Server and Client, although no functionality is currently built.

