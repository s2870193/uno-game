package main.Client.View.GUI;

import main.Server.Model.game.Game;
import main.Server.Model.player.Player;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;

public class MultiPlayer extends JFrame {
    private JTextArea textArea2;
    private JTextField userNameMultiPlayer;
    private JButton userNameMultiPlayerAccept;
    private JTextField consoleCommandsMP;
    private JPanel multiPlayer;
    private JTextArea consoleAreaMP;

    public String name;

    private ArrayList<Player> players;

    private Game game;

    public MultiPlayer () throws InterruptedException {

        setContentPane (multiPlayer);

        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

        setResizable (false);

        setTitle ("-Java® Uno Game-");

        setSize (700, 560);
        setLocationRelativeTo (null);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());

        consoleAreaMP.setEditable (false);
        consoleAreaMP.setAutoscrolls (true);


        PrintStream printStreamMP = new PrintStream (new CustomOutputStream (consoleAreaMP));

        //standardOut = System.out;

        System.setOut (printStreamMP);

        setVisible (true);
        consoleCommandsMP.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                System.out.println (consoleCommandsMP.getText ());
                if(consoleCommandsMP.getText ().equals ("/exit")) {
                    dispose ();
                } else if (consoleCommandsMP.getText ().equals ("/menu")) {
                    setVisible (false);
                    new MainMenu ();
                } else if (consoleCommandsMP.getText ().equals ("/settings")) {
                    setVisible (false);
                    new Settings ();
                } else if (consoleCommandsMP.getText ().equals ("/sp")) {
                    setVisible (false);
                    try {
                        new SinglePlayer ();
                    } catch (InterruptedException ex) {
                        throw new RuntimeException (ex);
                    }
                } else if(consoleCommandsMP.getText ().equals ("/start")){
                    try {
                    } catch (InterruptedException ex) {
                        throw new RuntimeException (ex);
                    }
                    consoleCommandsMP.setText ("");
                }
            }
        });
        userNameMultiPlayer.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                name = userNameMultiPlayer.getText ();
                userNameMultiPlayer.setEditable (false);
                userNameMultiPlayerAccept.setEnabled (false);
                System.out.println ("Welcome: " +name);
            }
        });
        userNameMultiPlayerAccept.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                name = userNameMultiPlayer.getText ();
                userNameMultiPlayer.setEditable (false);
                userNameMultiPlayerAccept.setEnabled (false);
            }
        });
    }

    public static void main (String[] args) {
        SwingUtilities.invokeLater (new Runnable () {
            @Override
            public void run () {
                try {
                    new MultiPlayer ().setVisible (true);
                } catch (InterruptedException e) {
                    throw new RuntimeException (e);
                }
            }
        });
    }


}