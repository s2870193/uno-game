package main.Client.View.GUI;

import javax.swing.*;

public class Settings extends JFrame {


    private JSlider fontSlider;
    private JRadioButton grayColor;
    private JRadioButton blackColor;
    private JComboBox comboBox1;
    private JPanel settings;
    private JLabel exampleTest;

    public Settings() {
        setContentPane (settings);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setResizable (false);
        setTitle ("-Java® Uno Game-");
        setSize (300, 220);
        setLocationRelativeTo (null);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());

        setVisible (true);
    }
}
