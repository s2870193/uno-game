package main.Client.View.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenu extends JFrame{
    private JPanel panel1;
    private JButton singlePlayerButton;
    private JButton multiplayerButton;
    private JButton settingsButton;
    private JButton exitButton;
    private JLabel welcome;
    private JLabel backgroundImage;

    public MainMenu() {
        setContentPane (panel1);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setResizable (false);
        setTitle ("-Java® Uno Game-");
        setSize (420, 620);
        setLocationRelativeTo (null);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());

        backgroundImage.setVisible (true);
        backgroundImage.setIcon (new ImageIcon ("main/Client/View/GUI/background_menu.jpg"));

        setVisible (true);


        singlePlayerButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                try {
                    SinglePlayer SP = new SinglePlayer ();
                } catch (InterruptedException ex) {
                    throw new RuntimeException (ex);
                }
            }
        });
        multiplayerButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                try {
                    MultiPlayer MP = new MultiPlayer ();
                } catch (InterruptedException ex) {
                    throw new RuntimeException (ex);
                }
            }
        });
        settingsButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                Settings ST = new Settings ();
            }
        });
        exitButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                dispose ();
            }
        });
    }



    public static void main (String[] args) {
        try {
            UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
        } catch (Exception ex) {
            ex.printStackTrace ();
        }
        Runnable r = new Runnable () {
            @Override
            public void run () {
               new MainMenu ();
            }
        };
        EventQueue.invokeLater (r);
    }
}