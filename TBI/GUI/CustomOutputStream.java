package main.Client.View.GUI;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;

public class CustomOutputStream extends OutputStream {
    private final JTextArea consoleArea;

    public CustomOutputStream (JTextArea consoleArea) {
        this.consoleArea = consoleArea;
    }

    @Override
    public void write (int b) throws IOException {
        consoleArea.append (String.valueOf ((char) b));
        consoleArea.setCaretPosition (consoleArea.getDocument ().getLength ());
    }
}
