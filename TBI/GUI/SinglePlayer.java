package main.Client.View.GUI;

/*
Java packages
 */


import main.Server.Model.game.Game;
import main.Server.Model.player.HumanPlayer;
import main.Server.Model.player.Player;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class SinglePlayer extends JFrame {
    private JTextField usernameField;
    private JButton usernameAccept;
    private JPanel singlePlayer;
    private JTextArea consoleArea;
    private JTextField consoleCommandSP;
    private JButton sendCommandSP;
    private JScrollPane consoleScrollPanel;

    private ArrayList<Player> players;

    private Game game;

    public static boolean start = false;

    public String name;

    public SinglePlayer () throws InterruptedException {
        players = new ArrayList<> ();
        setContentPane (singlePlayer);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setResizable (false);
        setTitle ("-Java® Uno Game-");
        setSize (600, 420);
        consoleArea.setEditable (false);
        consoleArea.setAutoscrolls (true);
        setLocationRelativeTo (null);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());

        /*
        JTextArea to cosole display.
         */

        PrintStream printStreamSP = new PrintStream (new CustomOutputStream (consoleArea));

        //standardOut = System.out;

        System.setOut (printStreamSP);

        setVisible (true);

        //////////


        consoleCommandSP.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                System.out.println (consoleCommandSP.getText ());
                if (consoleCommandSP.getText ().equals ("/exit")) {
                    dispose ();
                } else if (consoleCommandSP.getText ().equals ("/menu")) {
                    setVisible (false);
                    new MainMenu ();
                } else if (consoleCommandSP.getText ().equals ("/settings")) {
                    setVisible (false);
                    new Settings ();
                } else if (consoleCommandSP.getText ().equals ("/mp")) {
                    setVisible (false);
                    try {
                        new MultiPlayer ();
                    } catch (InterruptedException ex) {
                        throw new RuntimeException (ex);
                    }
                } else if (consoleCommandSP.getText ().equals ("/start")) {
                    System.out.println ("Welcome to the Single Player section!");
                    try {
                        System.out.println ("How many players do you want to add?");
                        Scanner playersAdded = new Scanner (System.in);
                        playersAdded.next ();
                        Thread.sleep (1000);
                    } catch (InterruptedException ex) {
                        throw new RuntimeException (ex);
                    }
                    try {
                        System.out.print ("Loading: [ ");
                        Thread.sleep (1000);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.print (" . ");
                        Thread.sleep (250);
                        System.out.println (" ]");
                        System.out.println ("STARTING!");
                        Thread.sleep (2000);
                        System.out.flush ();
                    } catch (InterruptedException ex) {
                        throw new RuntimeException (ex);
                    }
                } else {
                    consoleCommandSP.setText ("");
                }
            }
        });

        usernameField.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                name = usernameField.getText ();
                usernameField.setEditable (false);
                usernameAccept.setEnabled (false);
            }
        });
        usernameAccept.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                name = usernameField.getText ();
                players.add (new HumanPlayer (name));

                usernameField.setEditable (false);
                usernameAccept.setEnabled (false);
            }
        });
    }

    public static void main (String[] args){


        SwingUtilities.invokeLater (new Runnable () {
            @Override
            public void run () {
                try {
                    new SinglePlayer ().setVisible (true);
                } catch (InterruptedException e) {
                    throw new RuntimeException (e);
                }
            }
            });
        }
    }
